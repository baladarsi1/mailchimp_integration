<?php
/**
 * Created by PhpStorm.
 * User: balakrishna
 * Date: 8/14/2016
 * Time: 3:12 PM
 */
?>

<html>
<head>
    <title>Mailchimp Integration</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/jumbotron-narrow.css') }}" rel="stylesheet">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills pull-right">
                <li role="presentation" class="active"><a href="{{ url('/') }}/list">View Lists</a></li>
                <li role="presentation"><a href="{{ url('/') }}/list/create">Create New List</a></li>
            </ul>
        </nav>
        <h3 class="text-muted">Mail Chimp Integration</h3>
    </div>

    <div class="jumbotron">