@include('template/header');

<h4>Create New List</h4>

{{ FORM::open(array('url' => 'list')) }}


<div class="row">
    <h5>List Details</h5>
    <br/>
    <div class="col-md-6">
        List Name :
    </div>
    <div class="col-md-6">
        <input type="text"  name="name" />
    </div>
</div>

<div class="row">
    <h5>List Contact Details</h5>
    <br/>
    <div class="col-md-6">
        company :
    </div>
    <div class="col-md-6">
        <input type="text"  name="company" />
    </div>
    <br/>
    <div class="col-md-6">
        address1 :
    </div>
    <div class="col-md-6">
        <input type="text"  name="address1" />
    </div>
    <br/>
    <div class="col-md-6">
        address2 :
    </div>
    <div class="col-md-6">
        <input type="text"  name="address2" />
    </div>
    <br/>
    <div class="col-md-6">
        city :
    </div>
    <div class="col-md-6">
        <input type="text"  name="city" />
    </div>
    <br/>
    <div class="col-md-6">
        state :
    </div>
    <div class="col-md-6">
        <input type="text"  name="state" />
    </div>
    <br/>
    <div class="col-md-6">
        zip :
    </div>
    <div class="col-md-6">
        <input type="text"  name="zip" />
    </div>
    <br/>
    <div class="col-md-6">
        country :
    </div>
    <div class="col-md-6">
        <input type="text"  name="country" />
    </div>
    <br/>
    <div class="col-md-6">
        phone :
    </div>
    <div class="col-md-6">
        <input type="text"  name="phone" />
    </div>
    <h5>List Permission Reminder Details</h5>
    <br/>
    <div class="col-md-6">
        permission_reminder :
    </div>
    <div class="col-md-6">
        <input type="text"  name="permission_reminder" />
    </div>
    <h5>List Campaign Defaults Details</h5>
    <br/>
    <div class="col-md-6">
        from_name :
    </div>
    <div class="col-md-6">
        <input type="text"  name="from_name" />
    </div>
    <br/>
    <div class="col-md-6">
        from_email :
    </div>
    <div class="col-md-6">
        <input type="text"  name="from_email" />
    </div>
    <br/>
    <div class="col-md-6">
        subject :
    </div>
    <div class="col-md-6">
        <input type="text"  name="subject" />
    </div>
    <br/>
    <div class="col-md-6">
        language :
    </div>
    <div class="col-md-6">
        <input type="text"  name="language" />
    </div>
</div>

<br />

<div class="row">
    <div class="col-md-12 text-center">
        <input type="submit" name="submit" class="btn btn-md btn-primary" value="Create List"/>
    </div>
</div>

{{ Form::close() }}

@include('template/footer');