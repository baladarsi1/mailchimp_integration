@include('template/header');

<h4>All Lists</h4>
<br />
<br />
<table class="table">
     <thead>
        <th>
            List Id
        </th>
        <th>
            List Name
        </th>
        <th>
            Created Date
        </th>
        <th>
            Edit
        </th>
        <th>
            Delete
        </th>
        <th>
         List Members
        </th>
     </thead>
     <tbody>
          @foreach($all_list as $list)
              <tr>
                  <td>{{ $list->id }}</td>
                  <td>{{ $list->name }}</td>
                  <td>{{ $list->date_created }}</td>
                  <td>
                      <a href="{{ url('/') }}/list/{{ $list->id }}/edit"><span class="btn btn-success">Edit</span></a>
                  </td>
                  <td>
                      {{ FORM::open(array('url' => 'list/' . $list->id, 'class' => 'pull-right')) }}
                      {{ FORM::hidden('_method', 'DELETE') }}
                      {{ FORM::submit('Delete', array('class' => 'btn btn-warning')) }}
                      {{ FORM::close() }}

                  </td>
                  <td>
                      <a href="{{ url('/') }}/list_members/{{ $list->id }}"><span class="btn btn-success">View Members</span></a>
                  </td>
              </tr>
           @endforeach
     </tbody>
</table>

@include('template/footer');