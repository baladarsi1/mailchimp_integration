@include('template/header');

<h1>Edit List</h1>

{{ FORM::model($single_list, array('route' => array('list.update', $single_list->id), 'method' => 'PUT')) }}


 <div class="row">
     <div class="col-md-6">
         List Name :
     </div>
     <div class="col-md-6">
         <input type="text" value="{{ $single_list->name }}" name="name" />
     </div>
 </div>

 <br />

 <div class="row">
        <div class="col-md-12 text-center">
            <input type="submit" name="submit" class="btn btn-md btn-primary" value="Update List"/>
        </div>
  </div>

{{ Form::close() }}

@include('template/footer');