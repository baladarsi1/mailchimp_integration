@include('template/header');

<div class="row">
    <div class="col-md-8">
        <h4>All Lists (List Id : {{ $list_id  }})</h4>
    </div>
    <div class="col-md-4">
        <a href="{{ url('/') }}/create_list_member/{{ $list_id }}"><span class="btn btn-large btn-primary"> New Member </span> </a>
    </div>
</div>


<table class="table">
     <thead>
        <th>
            Member Email
        </th>
        <th>
            Status
        </th>
        <th>
            Edit
        </th>
        <th>
            Delete
        </th>
     </thead>
     <tbody>
          @foreach($all_list_members as $list_member)
              <tr>
                  <td>{{ $list_member->email_address }}</td>
                  <td>{{ $list_member->status }}</td>
                  <td>
                      <a href="{{ url('/') }}/edit_list_member/{{ $list_id }}/{{ $list_member->id }}"><span class="btn btn-success">Edit</span></a>
                  </td>
                  <td>
                      <a href="{{ url('/') }}/delete_list_member/{{ $list_id }}/{{ $list_member->id }}"><span class="btn btn-danger">Delete</span></a>
                  </td>
              </tr>
           @endforeach
     </tbody>
</table>

@include('template/footer');