@include('template/header');

<h4>Editing List Member (List Id : {{ $list_id }}) (List Member Id : {{ $list_member_id }})</h4>

{{ FORM::open(array('url' => 'edit_list_member', 'method' => 'post')) }}

<br />

<div class="row">

    <div class="col-md-6">
        status :
    </div>
    <div class="col-md-6">
        <input type="text"  name="status" value="{{ $single_list_member->status }}" />
    </div>
    <br />
    <div class="col-md-6">
        email_address :
    </div>
    <div class="col-md-6">
        <input type="text"  name="email_address" value="{{ $single_list_member->email_address }}" />
    </div>

</div>

<br />

<div class="row">
    <div class="col-md-12 text-center">
        <input type="submit" name="submit" class="btn btn-md btn-primary" value="Update List Member"/>
    </div>
</div>
<input type="hidden" name="list_id" value="{{ $list_id }}" />
<input type="hidden" name="list_member_id" value="{{ $list_member_id }}" />

{{ FORM::close() }}

@include('template/footer');