@include('template/header');

<h4>Create New List Member (List Id : {{ $list_id  }})</h4>

<br />

{{ FORM::open(array('url' => 'create_list_member', 'method' => 'post')) }}


<div class="row">

    <div class="col-md-6">
        status :
    </div>
    <div class="col-md-6">
        <input type="text"  name="status" />
    </div>
    <br />
    <div class="col-md-6">
        email_address :
    </div>
    <div class="col-md-6">
        <input type="text"  name="email_address" />
    </div>

</div>

<br />

<div class="row">
    <div class="col-md-12 text-center">
        <input type="submit" name="submit" class="btn btn-md btn-primary" value="Create List Member"/>
    </div>
</div>
 <input type="hidden" name="list_id" value="{{ $list_id }}" />

{{ FORM::close() }}

@include('template/footer');