<?php
/**
 * Created by PhpStorm.
 * User: balakrishna
 * Date: 8/13/2016
 * Time: 3:17 PM
 */

namespace App\Libraries;
use GuzzleHttp\Client;


class guzzle
{
    private $mailchimp_api_key = "1ccbfdbff4f724e5c586969359dc6961";

    public function create_get_request($method,$url) {

        $return = array('error' => 0, 'message' => '');

        $client = new Client([
            'headers' => [
                'content-type' => 'application/json'
            ],
        ]);

        $client->setDefaultOption('verify', false);

        $res = $client->$method($url, [
            'auth' => ['user', $this->mailchimp_api_key]
        ]);

        if($res->getStatusCode() == 200)
        {
            $body = $res->getBody();
            $return['error'] = 1;
            $return['message'] = json_decode($body);
        }

        else {

            $return['error'] = 0;
            $return['message'] = "!oops the http request is not successfull";
        }

        return $return;

    }

    public function create_post_request($method,$url,$data=array()) {

        $return = array('error' => 0, 'message' => '');

        $client = new Client([
            'headers' => [
                'content-type' => 'application/json'
            ],
        ]);

        $client->setDefaultOption('verify', false);

        $res = $client->$method($url, [
            'auth' => ['user', $this->mailchimp_api_key],
            'body' => json_encode($data)
        ]);

        if($res->getStatusCode() == 200)
        {
            $body = $res->getBody();
            $return['error'] = 1;
            $return['message'] = json_decode($body);
        }

        else {

            $return['error'] = 0;
            $return['message'] = "!oops the http request is not successfull";
        }

        return $return;

    }
}