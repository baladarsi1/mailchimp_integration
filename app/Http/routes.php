<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//create route for mail chimp requests
Route::resource('list', 'MailchimpController');

//create route for mail chimp list members
Route::get('list_members/{list_id}', 'MembersController@list_members');

//create new list member
Route::get('create_list_member/{list_id}', 'MembersController@create_list_member');

//submit new list member
Route::post('create_list_member', 'MembersController@store');

//edit list member
Route::get('edit_list_member/{list_id}/{list_member_id}', 'MembersController@edit');

//submit list member
Route::post('edit_list_member', 'MembersController@update');

//delete list member
Route::get('delete_list_member/{list_id}/{list_member_id}', 'MembersController@destroy');



Route::auth();

Route::get('/home', 'HomeController@index');
