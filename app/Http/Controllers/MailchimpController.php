<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Libraries;
use Illuminate\Support\Facades\View;

class MailchimpController extends Controller
{
    private $mailchimp_api_url = "https://us1.api.mailchimp.com/3.0";

    public function all_lists()
    {
        return $this->mailchimp_api_url."/lists";
    }

    public function single_list($list_id)
    {
        return $this->mailchimp_api_url."/lists/".$list_id;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = new Libraries\guzzle();
        $mailchimp_list = $request->create_get_request('get',$this->all_lists());
        $return_array = array(
           'all_list' => $mailchimp_list ['message']->lists
        );

        return view('list.index' , $return_array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('list.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req = new Libraries\guzzle();
        $update_data = array(
            'name' => $request->input('name'),
            'contact' => (object)array(
                "company"=>$request->input('company'),
                "address1" => $request->input('address1'),
                "address2" => $request->input('address2'),
                "city" => $request->input('city'),
                "state" => $request->input('state'),
                "zip" => $request->input('zip'),
                "country" => $request->input('country'),
                "phone" => $request->input('phone')
            ),
            'permission_reminder' => "receiving this email because you signed up for updates about ",
            'campaign_defaults' => (object)array(
                "from_name"=>$request->input('from_name'),
                "from_email" => $request->input('from_email'),
                "subject" => $request->input('subject'),
                "language" => $request->input('language')
            ),
            'email_type_option' => false
        );


        $mailchimp_list = $req->create_post_request('post',$this->all_lists(),$update_data);

        if($mailchimp_list ['error'] == 1)
        {
            return redirect('list');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $request = new Libraries\guzzle();
        $mailchimp_list = $request->create_get_request('get',$this->single_list($id));

        $return_array = array(
            'single_list' => $mailchimp_list ['message']
        );

        return view('list/edit' , $return_array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $req = new Libraries\guzzle();
        $update_data = array(
            'name' => $request->input('name')
        );

        $mailchimp_list = $req->create_post_request('patch',$this->single_list($id),$update_data);

        if($mailchimp_list ['error'] == 1)
        {
            return redirect('list');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = new Libraries\guzzle();
        $mailchimp_list = $request->create_get_request('delete',$this->single_list($id));

        return redirect('list');


    }
}
