<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Libraries;
use Illuminate\Support\Facades\View;

class MembersController extends Controller
{
    private $mailchimp_api_url = "https://us1.api.mailchimp.com/3.0";

    public function all_list_members($list_id)
    {
        return $this->mailchimp_api_url."/lists/".$list_id."/members";
    }

    public function list_member($list_id,$list_member_id)
    {
        return $this->mailchimp_api_url."/lists/".$list_id."/members/".$list_member_id;
    }

    //show all members in the list
    public function list_members($list_id)
    {

        $request = new Libraries\guzzle();
        $mailchimp_list = $request->create_get_request('get',$this->all_list_members($list_id));
        $return_array = array(
            'list_id' => $list_id,
            'all_list_members' => $mailchimp_list ['message']->members
        );

        return view('members.index' , $return_array);
    }

    /**
     * Create new list member
     */

    public function create_list_member($list_id)
    {
        $return_array = array(
            'list_id' => $list_id
        );

        return View::make('members.create' , $return_array);
    }

    /**
     * Store a newly created list member
     *
     */
    public function store(Request $request)
    {
        $list_id = $request->input('list_id');

        $req = new Libraries\guzzle();
        $update_data = array(
            'status' => $request->input('status'),
            'email_address' => $request->input('email_address')
        );

        $mailchimp_list = $req->create_post_request('post',$this->all_list_members($list_id),$update_data);

        if($mailchimp_list ['error'] == 1)
        {
            return redirect('list_members/'.$list_id);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit($list_id,$list_member_id)
    {
        $return_array = array(
            'list_id' => $list_id,
            'list_member_id' => $list_member_id
        );

        $request = new Libraries\guzzle();
        $mailchimp_list = $request->create_get_request('get',$this->list_member($list_id,$list_member_id));

        $return_array ['single_list_member'] = $mailchimp_list ['message'];

        return View::make('members.edit' , $return_array);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */

    public function update(Request $request)
    {
        $req = new Libraries\guzzle();
        $update_data = array(
            'status' => $request->input('status'),
            'email_address' => $request->input('email_address')
        );

        $mailchimp_list = $req->create_post_request('patch',$this->list_member($request->input('list_id'),$request->input('list_member_id')),$update_data);

        if($mailchimp_list ['error'] == 1)
        {
            return redirect('list_members/'.$request->input('list_id'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($list_id,$list_member_id)
    {
        $request = new Libraries\guzzle();
        $mailchimp_list = $request->create_get_request('delete',$this->list_member($list_id,$list_member_id));

        return redirect('list_members/'.$list_id);
    }
}
